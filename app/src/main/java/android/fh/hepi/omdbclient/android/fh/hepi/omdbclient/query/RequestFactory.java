package android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Felix on 27.10.2015.
 */
public class RequestFactory {

    public static String NAME_SEARCH_TEMPLATE = "http://www.omdbapi.com/?s=%s&r=json";
    public static String TITLE_SEARCH_TEMPLATE = "http://www.omdbapi.com/?t=%s&y=&plot=short&r=json";

    static InputStream getStream(String searchItem, String template){
        String query = wrapParameter(searchItem);
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(String.format(template,query));
            System.out.println("get:" + url.toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return in;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            urlConnection.disconnect();
        }
        return null;
    }

    private static String wrapParameter(String parameter) {
        String query = parameter.trim();
        query = query.replaceAll(" ", "%20");
        return query;
    }
}
