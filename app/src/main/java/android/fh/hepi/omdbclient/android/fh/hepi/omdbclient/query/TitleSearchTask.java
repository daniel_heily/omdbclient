package android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query;

import android.fh.hepi.omdbclient.Helper;
import android.fh.hepi.omdbclient.ISearchTaskListener;
import android.os.AsyncTask;

import com.google.gson.Gson;

/**
 * Created by Daniel on 05.11.2015.
 */
public class TitleSearchTask extends AsyncTask<SearchResponseItem, Integer, String> {

    private ISearchTaskListener listener;
    public TitleSearchTask(ISearchTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(SearchResponseItem... params) {
        String titleSearchResult = "";
        if(params[0] != null) {
            titleSearchResult = Helper.toString(RequestFactory.getStream(params[0].Title, RequestFactory.TITLE_SEARCH_TEMPLATE));
        }
        return titleSearchResult;
    }

    @Override
    protected void onPostExecute(String result) {
        Gson gson = new Gson();
        ShortResponse shortResponse = gson.fromJson(result, ShortResponse.class);
        listener.receiveTitleSearchResponse(shortResponse);
    }
}
