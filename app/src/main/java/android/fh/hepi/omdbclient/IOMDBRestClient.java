package android.fh.hepi.omdbclient;

import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.SearchResponse;
import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.ShortResponse;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.GsonHttpMessageConverter;


/**
 * Created by Daniel on 08.11.2015.
 */

@Rest(rootUrl = "http://www.omdbapi.com", converters = {GsonHttpMessageConverter.class })
public interface IOMDBRestClient {

    @Get("/?s={searchText}&r=json")
    SearchResponse getSearchResponse(String searchText);

    @Get("?t={title}&y=&plot=short&r=json")
    ShortResponse getDetailSerachResponse(String title);
}


