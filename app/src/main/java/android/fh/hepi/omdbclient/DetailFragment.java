package android.fh.hepi.omdbclient;

import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.ShortResponse;
import android.fh.hepi.omdbclient.generated.DaoSession;
import android.fh.hepi.omdbclient.generated.MovieDetails;
import android.fh.hepi.omdbclient.generated.MovieDetailsDao;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.InputStream;

@EFragment(R.layout.fragment_detail)
public class DetailFragment extends Fragment {

    @ViewById
    TextView titleLabel;
    @ViewById
    TextView yearLabel;
    @ViewById
    TextView runtimeLabel;
    @ViewById
    ImageView imageView;

    private ShortResponse shortResponse;

    private static final String LOG_TAG = "DetailFragment";

    public DetailFragment() {
        // Required empty public constructor
    }

    public void setShortResponse(ShortResponse shortResponse) {
        this.shortResponse = shortResponse;
    }

    @AfterViews
    public void initDetailView() {
        if(shortResponse != null) {
            new DownloadImageTask(imageView)
                    .execute(shortResponse.Poster);
            titleLabel.setText(shortResponse.Title);
            yearLabel.setText(shortResponse.Year);
            runtimeLabel.setText(shortResponse.Runtime);
            storeMovieDetails(shortResponse);
        }
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        super.onCreateView(inflater,container,savedInstanceState);
//        View view = inflater.inflate(R.layout.fragment_detail, container, false);
//        titleLabel = (TextView)view.findViewById(R.id.titleLabel);
//        yearLabel = (TextView)view.findViewById(R.id.yearLabel);
//        runtimeLabel = (TextView)view.findViewById(R.id.runtimeLabel);
//        imageView = (ImageView) view.findViewById(R.id.imageView);
//        new DownloadImageTask(imageView)
//                .execute(shortResponse.Poster);
//        titleLabel.setText(shortResponse.Title);
//        yearLabel.setText(shortResponse.Year);
//        runtimeLabel.setText(shortResponse.Runtime);
//        return view;
//    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView imageView;

        public DownloadImageTask(ImageView bmImage) {
            this.imageView = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }
    }

    private DaoSession getDaoSessionFromActivity(){
        return ((MainActivity)getActivity()).getDaoSession();
    }

    private void storeMovieDetails(ShortResponse response) {
        if(response == null) {
            return;
        }
        Log.i(LOG_TAG, "DAO: insert movie details" );

        MovieDetails details = new MovieDetails();
        details.setTitle(response.Title);
        details.setYear(response.Year);
        details.setRuntime(response.Runtime);
        details.setPoster(response.Poster);

        DaoSession session = getDaoSessionFromActivity();
        MovieDetailsDao dao = session.getMovieDetailsDao();
        dao.insert(details);

        Log.i(LOG_TAG, "DAO: insert movie details done" );
    }
}
