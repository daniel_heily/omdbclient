package android.fh.hepi.omdbclient;


import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.SearchResponse;
import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.ShortResponse;

/**
 * Created by Daniel on 04.11.2015.
 */
public interface ISearchTaskListener {
    public void receiveSearchResponse(SearchResponse response);
    public void receiveTitleSearchResponse(ShortResponse response);
}
