package android.fh.hepi.omdbclient;

import android.database.sqlite.SQLiteDatabase;
import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.SearchResponse;
import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.SearchResponseItem;
import android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.ShortResponse;
import android.fh.hepi.omdbclient.generated.DaoMaster;
import android.fh.hepi.omdbclient.generated.DaoSession;
import android.fh.hepi.omdbclient.generated.MovieDetails;
import android.fh.hepi.omdbclient.generated.MovieDetailsDao;
import android.fh.hepi.omdbclient.generated.SearchQuery;
import android.fh.hepi.omdbclient.generated.SearchQueryDao;
import android.fh.hepi.omdbclient.generated.SearchResponseItemDao;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.Date;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */

@EFragment(R.layout.fragment_main)
public class MainActivityFragment extends Fragment {

    // daos
    private DaoSession daoSession;
    private SearchQueryDao searchQueryDao;
    private SearchResponseItemDao searchResponseItemDao;
    private MovieDetailsDao movieDetailsDao;

    ArrayAdapter<android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.SearchResponseItem> adapter;

    @ViewById
    EditText txtQuery;
    //@ViewById
    //Button btnSearch;
    @ViewById
    ListView listView;
    @ViewById
    TextView empty;

    @RestService
    IOMDBRestClient restClient;

    private static final String LOG_TAG = "SearchFragment";
    private SearchQuery currentSearchQuery;

    public MainActivityFragment() {
    }

    @AfterViews
    public void init() {
        adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
        listView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        initORM();
    }

    @Click(R.id.btnSearch)
    public void startSearch() {
        searchAsync(txtQuery.getText().toString());
    }

    @ItemClick(R.id.listView)
    public void onItemClick(android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query.SearchResponseItem searchResponseItem) {
        if(searchResponseItem != null) {
            Log.i(LOG_TAG, String.format("clicked on item %s", searchResponseItem.toString()));
            searchDetailsAsync(searchResponseItem.Title);
        }
    }

    @Background
    public void searchAsync(String searchText) {
        SearchResponse response = restClient.getSearchResponse(searchText);
        showSearchResponse(response);
        insertSearchQuery(searchText);
        storeSearchResponse(response);
    }

    @UiThread
    public void showSearchResponse(SearchResponse response) {
        adapter.clear();
        if(response == null || response.results == null) {
            viewEmptySearchResult();
            return;
        }
        Log.i(LOG_TAG, "received response" + response.toString());
        for (SearchResponseItem item : response.results) {
            adapter.add(item);
        }
    }


    private void storeSearchResponse(SearchResponse response) {
        if (response == null || response.results == null) {
            return;
        }
        Log.i(LOG_TAG, "DAO: insert search response" );
        for (SearchResponseItem item : response.results) {
            insertSearchResponseItem(item);
        }

    }

    @Background
    public void searchDetailsAsync(String title) {
        ShortResponse response = restClient.getDetailSerachResponse(title);
        showDetailSearchResponse(response);
        // todo storeMovieDetails(response);
    }

    @UiThread
    public void showDetailSearchResponse(ShortResponse response) {
        DetailFragment detailFragment = new DetailFragment_();
        detailFragment.setShortResponse(response);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        ft.replace(R.id.container, detailFragment, getString(R.string.detailFragment));
        ft.commit();
    }

    private void viewEmptySearchResult() {
        Log.i(LOG_TAG, "received response is null or empty, set empty view" );
        empty.setText("No search results!" );
        listView.setEmptyView(empty);
    }

    @Click(R.id.btnCache)
    public void showCache() {
        Log.i(LOG_TAG, "showHistory" );
        List<SearchQuery> list = getSearchQueries();
        logHistory(list);
        showHistroy(list);


    }

    @UiThread
    public void showHistroy(List<SearchQuery> data) {
        HistoryFragment fragment = new HistoryFragment_();
        fragment.setData(getSearchQueries());
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        ft.replace(R.id.container, fragment, getString(R.string.historyFragment));
        ft.commit();
    }

    private void logHistory(List<SearchQuery> list) {
        if(list == null || list.size() <= 0){
            Log.i(LOG_TAG, "No History available!" );
            return;
        }
        SearchQuery item = list.get(list.size()-1);
        Log.i(LOG_TAG, "Last:" + item.getId() + item.getDate().toString() + item.getQuery());
        List<android.fh.hepi.omdbclient.generated.SearchResponseItem> response = item.getSearchResponseItemList();
        String resultString = "";
        for (android.fh.hepi.omdbclient.generated.SearchResponseItem resultItem:response) {
            resultString = resultString + resultItem.getTitle() + "\t";
        }
        Log.i(LOG_TAG, resultString);
        //adapter.clear();
        //empty.setText(resultString);
        //listView.setEmptyView(empty);
    }


    private void initORM() {
        daoSession = getDaoSessionFromActivity();
        searchQueryDao = daoSession.getSearchQueryDao();
        searchResponseItemDao = daoSession.getSearchResponseItemDao();
        movieDetailsDao = daoSession.getMovieDetailsDao();
    }



    private void insertSearchQuery(String query) {
        SearchQuery s = new SearchQuery();
        s.setDate(new Date());
        s.setQuery(query);
        searchQueryDao.insert(s);
        currentSearchQuery = s;
    }

    private void insertSearchResponseItem(SearchResponseItem searchResponseItem) {
        android.fh.hepi.omdbclient.generated.SearchResponseItem responseItem = new android.fh.hepi.omdbclient.generated.SearchResponseItem();
        responseItem.setTitle(searchResponseItem.Title);
        responseItem.setYear(searchResponseItem.Year);
        responseItem.setImdbID(searchResponseItem.imdbID);
        responseItem.setType(searchResponseItem.Type);
        responseItem.setPoster(searchResponseItem.Poster);
        responseItem.setSearchQueryId(currentSearchQuery.getId());
        searchResponseItemDao.insert(responseItem);
    }

    private List<SearchQuery> getSearchQueries() {
        return searchQueryDao.queryBuilder().listLazy();
    }




    private DaoSession getDaoSessionFromActivity(){
        return ((MainActivity)getActivity()).getDaoSession();
    }
}
