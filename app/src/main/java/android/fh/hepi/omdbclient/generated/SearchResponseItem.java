package android.fh.hepi.omdbclient.generated;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "SEARCH_RESPONSE_ITEM".
 */
public class SearchResponseItem {

    private Long id;
    private String title;
    private String year;
    private String imdbID;
    private String type;
    private String poster;
    private long searchQueryId;

    public SearchResponseItem() {
    }

    public SearchResponseItem(Long id) {
        this.id = id;
    }

    public SearchResponseItem(Long id, String title, String year, String imdbID, String type, String poster, long searchQueryId) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.imdbID = imdbID;
        this.type = type;
        this.poster = poster;
        this.searchQueryId = searchQueryId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public long getSearchQueryId() {
        return searchQueryId;
    }

    public void setSearchQueryId(long searchQueryId) {
        this.searchQueryId = searchQueryId;
    }

}
