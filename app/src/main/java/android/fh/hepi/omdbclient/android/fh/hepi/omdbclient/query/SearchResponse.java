package android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Felix on 27.10.2015.
 */
public class SearchResponse {

    @SerializedName("Search")
    public List<SearchResponseItem> results;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(SearchResponseItem item : results) {
            sb.append(" "+item.Title);
        }
        return sb.toString();
    }

}
