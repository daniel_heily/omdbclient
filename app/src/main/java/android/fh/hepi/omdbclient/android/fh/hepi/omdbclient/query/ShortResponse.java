package android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Felix on 27.10.2015.
 */
public class ShortResponse {
    @SerializedName("Title")
    public String Title;

    @SerializedName("Year")
    public String Year;

    @SerializedName("Rated")
    public String Rated;

    @SerializedName("Released")
    public String Released;

    @SerializedName("Runtime")
    public String Runtime;

    @SerializedName("Genre")
    public String Genre;

    @SerializedName("Director")
    public String Director;

    @SerializedName("Writer")
    public String Writer;

    @SerializedName("Actors")
    public String Actors;

    @SerializedName("Plot")
    public String Plot;

    @SerializedName("Language")
    public String Language;

    @SerializedName("Country")
    public String Country;

    @SerializedName("Awards")
    public String Awards;

    @SerializedName("Poster")
    public String Poster;

    @SerializedName("Metascore")
    public String Metascore;

    @SerializedName("imdbRating")
    public String imdbRating;

    @SerializedName("imdbVotes")
    public String imdbVotes;


    @SerializedName("imdbID")
    public String imdbID;

    @SerializedName("Type")
    public String Type;

    @SerializedName("Response")
    public String Response;

    public String toString() {
        return Title;
    }
}
