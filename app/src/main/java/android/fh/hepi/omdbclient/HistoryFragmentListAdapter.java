package android.fh.hepi.omdbclient;

import android.fh.hepi.omdbclient.generated.SearchQuery;
import android.fh.hepi.omdbclient.generated.SearchResponseItem;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by Felix on 09.11.2015.
 */
public class HistoryFragmentListAdapter extends RecyclerView.Adapter<HistoryFragmentListAdapter.ViewHolder>{

    private List<SearchQuery> mData;

    public HistoryFragmentListAdapter(List<SearchQuery> data) {
        this.mData = data;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        //@ViewById(R.id.fragment_history_item_item)
        public TextView textViewItem;
        //@ViewById(R.id.fragment_history_item_results)
        public TextView textViewResults;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            textViewItem = (TextView) itemLayoutView.findViewById(R.id.fragment_history_item_item);
            textViewResults = (TextView) itemLayoutView.findViewById(R.id.fragment_history_item_results);

        }

    }

    @Override
    public HistoryFragmentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_history_item, null);

        // create ViewHolder

        ViewHolder viewHolder = new HistoryFragmentListAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        SearchQuery q = mData.get(position);

        viewHolder.textViewItem.setText("Search Query # " + mData.get(position).getId() + "\n" +
                q.getDate().toLocaleString() + "\n '" +
                q.getQuery() +"'");

        List<SearchResponseItem> resultList = mData.get(position)
                .getSearchResponseItemList();
        String resultsAsText = "Results:\n";
        for (SearchResponseItem result : resultList   ) {
            resultsAsText += " - " + result.getTitle() + "(" + result.getTitle() +")\n";
        }
        viewHolder.textViewResults.setText(resultsAsText);
    }
}
