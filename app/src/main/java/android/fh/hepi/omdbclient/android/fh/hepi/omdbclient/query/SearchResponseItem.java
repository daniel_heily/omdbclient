package android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Felix on 27.10.2015.
 */
public class SearchResponseItem {
    @SerializedName("Title")
    public String Title;

    @SerializedName("Year")
    public String Year;

    @SerializedName("imdbID")
    public String imdbID;

    @SerializedName("Type")
    public String Type;

//    @SerializedName("Runtime")
//    public String Runtime;

    @SerializedName("Poster")
    public String Poster;

    public String toString() {
        return Title;
    }

}
