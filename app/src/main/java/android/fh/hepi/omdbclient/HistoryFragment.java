package android.fh.hepi.omdbclient;

import android.fh.hepi.omdbclient.generated.SearchQuery;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EFragment(R.layout.fragment_history)
public class HistoryFragment extends Fragment {

    private List<SearchQuery> data;

    public void setData(List<SearchQuery> data) {
        this.data = data;
    }

    @ViewById(R.id.fragment_history_recyclerView)
    RecyclerView recyclerView;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @AfterViews
    public void initHistoryView() {
        if (data != null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(
                    getContext(),
                    LinearLayoutManager.VERTICAL,
                    false);
            layoutManager.setReverseLayout(true);
            layoutManager.setStackFromEnd(true);
            recyclerView.setLayoutManager(layoutManager);
            HistoryFragmentListAdapter adapter = new HistoryFragmentListAdapter(data);
            recyclerView.setAdapter(adapter);
        }
    }
}
