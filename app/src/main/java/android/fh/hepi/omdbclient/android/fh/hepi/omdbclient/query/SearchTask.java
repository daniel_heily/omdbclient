package android.fh.hepi.omdbclient.android.fh.hepi.omdbclient.query;

import android.fh.hepi.omdbclient.Helper;
import android.fh.hepi.omdbclient.ISearchTaskListener;
import android.os.AsyncTask;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.InputStream;

/**
 * Created by Daniel on 04.11.2015.
 */
public class SearchTask extends AsyncTask<String, Integer, String> {

    ISearchTaskListener listener;
    public SearchTask(ISearchTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        InputStream in = RequestFactory.getStream(params[0], RequestFactory.NAME_SEARCH_TEMPLATE);
        String inputString = Helper.toString(in);
        return inputString;
    }

    @Override
      protected void onPostExecute(String result) {
        Gson gson = new Gson();
        SearchResponse gsonContent = gson.fromJson(result, SearchResponse.class);
        listener.receiveSearchResponse(gsonContent);
    }
}