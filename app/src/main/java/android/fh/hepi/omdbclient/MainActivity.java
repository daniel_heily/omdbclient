package android.fh.hepi.omdbclient;

import android.database.sqlite.SQLiteDatabase;
import android.fh.hepi.omdbclient.generated.DaoMaster;
import android.fh.hepi.omdbclient.generated.DaoSession;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    private static final String DATABASE_NAME = "greendao-search-query-cache";

    private SQLiteDatabase databaseConnection;
    private DaoMaster daoMaster;
    private DaoSession daoSession;

    public DaoSession getDaoSession() {
        return daoSession;
    }

    @AfterViews
    public void init() {
        initORM();
        if (findViewById(R.id.container) != null)
            if (getFragmentManager().findFragmentByTag("mainFragment") == null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.container, new MainActivityFragment_(), getString(R.string.mainFragment));
                ft.commit();
            }
    }

    private void initORM() {
        databaseConnection = new DaoMaster.DevOpenHelper(this, DATABASE_NAME, null)
                .getWritableDatabase();
        daoMaster = new DaoMaster(databaseConnection);
        daoSession = daoMaster.newSession();
    }

    private void stopORM() {
        if (databaseConnection != null && databaseConnection.isOpen()) {
            databaseConnection.close();
        }
    }
}
