package fh.hepi.orm;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class Generator{
    private static final String TARGET_FOLDER = "../app/src/main/java/";
    private static final int SCHEMA_VERSION = 1;
    private Schema schema = new Schema(SCHEMA_VERSION, "android.fh.hepi.omdbclient.generated");

    private void generateSchema() throws Throwable {
        Entity searchQuery = schema.addEntity("SearchQuery");
        searchQuery.addIdProperty();
        searchQuery.addDateProperty("date" );
        searchQuery.addStringProperty("query" );

        Entity searchResponseItem = schema.addEntity("SearchResponseItem");
        searchResponseItem.addIdProperty();
        searchResponseItem.addStringProperty("title");
        searchResponseItem.addStringProperty("year");
        searchResponseItem.addStringProperty("imdbID");
        searchResponseItem.addStringProperty("type");
        searchResponseItem.addStringProperty("poster");

        Property searchQueryId = searchResponseItem.addLongProperty("searchQueryId").notNull().getProperty();

        searchQuery.addToMany(searchResponseItem, searchQueryId);

        Entity movieDetails = schema.addEntity("MovieDetails");
        movieDetails.addIdProperty();
        movieDetails.addStringProperty("title");
        movieDetails.addStringProperty("year");
        movieDetails.addStringProperty("runtime");
        movieDetails.addStringProperty("poster");


        new DaoGenerator().generateAll(schema, TARGET_FOLDER);
    }

    public static final void main(String[] args) throws Throwable {
        new Generator().generateSchema();
    }
}
